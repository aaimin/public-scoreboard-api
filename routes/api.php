<?php

use Illuminate\Http\Request;

Route::middleware(['auth:api'])->group(function () {

    Route::apiResources([
        'metric' => 'MetricApiController',
        'scoreboard-review' => 'ScoreboardReviewApiController'
    ]);

    Route::prefix('new')->group(function () {

        Route::get('metric', 'MetricIndexApiController');
        Route::get('metric-action', 'MetricActionIndexApiController');
        Route::get('user', 'UserIndexApiController');
        Route::get('metric-aggregate', 'MetricAggregateIndexApiController');
        Route::post('metric-aggregate', 'MetricAggregateStoreApiController');
        Route::patch('metric-aggregate/{id}/edit', 'MetricAggregateUpdateApiController');
        Route::delete('metric-aggregate/{id}', 'MetricAggregateDestroyApiController');
    });

    Route::get('user', 'PassportApiController@user');
    Route::get('logout', 'PassportApiController@logout');

    Route::get('report/scoreboard-submissions', 'ReportScoreboardSubmissionsIndexApiController');
    Route::get('report/progress-summary', 'ReportProgressSummaryIndexApiController');

    Route::get('actions/showUserMetrics', 'MetricWithRelationshipsIndexApiController');
    Route::post('actions/storeUserMetrics', 'MetricWithRelationshipsUpdateApiController');

    Route::get('group-users', 'GroupUserIndexApiController');
    Route::get('scoreboard', 'ScoreboardIndexApiController');

    Route::post('scoreboard-checkin-mail', 'ScoreboardCheckinMailUpdateApiController');

    Route::get('user-metric-track', 'MetricTrackIndexApiController');

    Route::post('metric-action/update-or-create', 'MetricActionUpdateApiController');
    Route::post('complete-metric-action', 'MetricActionCompleteUpdateApiController');
    Route::post('hide-metric-action', 'MetricActionHideUpdateApiController');
    Route::post('unhide-metric-action', 'MetricActionUnhideUpdateApiController');
    Route::delete('metric-action/{id}', 'MetricActionDestroyApiController');

    Route::get('metric-query-values', 'MetricQueryValueIndexApiController');
    Route::get('metric-query-detail', 'MetricQueryDetailIndexApiController');
    Route::get('metric-aggregate-values', 'MetricAggregateValueIndexApiController');
    Route::get('metric-aggregate-detail', 'MetricAggregateDetailIndexApiController');

    Route::patch('scoreboard-review/{id}/approve', 'ScoreboardReviewApiController@approve');
    Route::patch('scoreboard-review/{id}/reject', 'ScoreboardReviewApiController@reject');
    Route::get('feedback-question', 'FeedbackQuestionIndexApiController');
    Route::post('multiple-feedback-answer', 'FeedbackAnswerStoreApiController');
});

Route::post('register', 'PassportApiController@register');
Route::post('login', 'PassportApiController@login');

Route::get('bitrix-user', 'BitrixUserIndexApiController');
Route::get('filter/user', 'FilterUserIndexApiController');
Route::get('filter/user-group', 'FilterUserGroupIndexApiController');
Route::get('filter/business-unit', 'FilterBusinessUnitIndexApiController');
