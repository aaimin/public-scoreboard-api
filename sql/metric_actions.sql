
DROP TABLE IF EXISTS `metric_actions`;
CREATE TABLE `metric_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `week_id` int(10) NOT NULL,
  `ref_metric` int(10) NOT NULL,
  `type` varchar(30) NOT NULL,
  `capa` text NOT NULL,
  `accountable` varchar(255) NOT NULL,
  `responsible` varchar(255) NOT NULL,
  `deadline` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
