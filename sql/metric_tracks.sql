
DROP TABLE IF EXISTS `metric_tracks`;
CREATE TABLE `metric_tracks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ref_metric` int(10) NOT NULL,
  `week_id` int(10) NOT NULL,
  `track_value` double(10,2) DEFAULT NULL,
  `target_value` double(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;