
DROP TABLE IF EXISTS `metrics`;
CREATE TABLE `metrics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ref_metric` int(10) NOT NULL,
  `ref_wig` int(10) NOT NULL,
  `ref_lag` int(10) DEFAULT NULL,
  `ref_lead` int(10) DEFAULT NULL,
  `ref_act` int(10) DEFAULT NULL,
  `ref_qact` int(10) DEFAULT NULL,
  `name` text NOT NULL,
  `type_id` int(10) NOT NULL,
  `type_code` varchar(5) NOT NULL,
  `target_unit` varchar(5) DEFAULT NULL,
  `target_value` double(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
