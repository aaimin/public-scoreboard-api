
DROP TABLE IF EXISTS `user_details`;
CREATE TABLE `user_details` (
  `id` bigint(20) unsigned NOT NULL,
  `gp_id` varchar(10) NOT NULL,
  `business_unit` varchar(30) NOT NULL,
  `group` varchar(30) NOT NULL,
  `function` varchar(30) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
