<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FeedbackQuestion;

class FeedbackQuestionIndexApiController extends Controller {

    public function __invoke(Request $request) {

        $feedback_question = FeedbackQuestion::where('is_active', 1)
            ->get();

        return [
            'data' => [
                'feedback_question'
                    => $feedback_question
            ]
        ];
    }
}
