<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PassportApiController extends Controller {

    public function register (Request $request) {

    }

    public function login (Request $request) {

        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);
            
        $email = $request->input('email');
        $password = $request->input('password');
        $is_new = $request->input('is_new');

        if ($is_new) {

            $user = DB::table('users')
                ->where('email', $email)
                ->where(function ($query) {
                    $query->where('password', '')
                        ->orWhereNull('password');
                })
                ->first();

            if (!$user) {

                return response()->json([
                    'status' => 'error'
                ]);
            } 

            DB::table('users')
                ->where('email', $email)
                ->update(['password' => bcrypt($password)]);
        }

        $credentials = [
            'email' => $email,
            'password' => $password
        ];

        if (!auth()->attempt($credentials)) {
            return response()->json([], 401);
        }

        $token = auth()
            ->user()
            ->createToken('scoreboard')
            ->accessToken;

        $admin_count = DB::table('pn_user_user_groups')
            ->where('user_id', auth()->user()->id)
            ->where('is_admin', 1)
            ->count();
        
        return response()->json([
            'user' => auth()->user(),
            'token' => $token,
            'is_admin' => $admin_count > 0
        ]);
    }

    public function user () {

        return response()->json([
            'user' => auth()->user()
        ]);
    }

    public function logout () {
        auth()->user()
            ->token()
            ->revoke();

        return response()->json();
    }
}
