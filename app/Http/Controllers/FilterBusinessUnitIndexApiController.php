<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class FilterBusinessUnitIndexApiController extends Controller {

    public function __invoke(Request $request) {

        $collection = DB::table('pn_user_groups')
            ->distinct()
            ->select('business_unit')
            ->orderBy('business_unit')
            ->get();

        return ['items' => $collection];
    }
}
