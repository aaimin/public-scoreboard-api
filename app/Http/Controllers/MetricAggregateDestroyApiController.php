<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MetricAggregateDestroyApiController extends Controller {

    function __invoke (Request $request, $id) {

        DB::table('pn_metric_aggregates')
            ->where('id', $id)
            ->delete();

        return ['status' => 'success'];
    }
}
