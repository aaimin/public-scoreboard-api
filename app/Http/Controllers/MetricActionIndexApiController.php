<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MetricActionIndexApiController extends Controller {

    public function __invoke(Request $request) {

        $user = auth()->user();
        $week_id = $request->query('week_id');
        $cycle_name = '2019-Q' . (int) ceil($week_id/13);

        $metric_lag_list = DB::table('metrics')
            ->select(
                'id',
                'cycle_name',
                'ref_metric',
                'name'
            )
            ->where('user_id', $user->id)
            ->where('type_code', 'lag')
            ->orderBy('cycle_name', 'desc')
            ->orderBy('id')
            ->get();

        $metric_action_list = DB::table('metric_actions')
            ->where('user_id', $user->id)
            // ->where('cycle_name', $cycle_name)
            // ->where('week_id', '<=', $week_id)
            // ->where(function ($query) use ($week_id) {
            //     $query->whereNull('hide_week_id')
            //         ->orWhere('hide_week_id', '>=', $week_id);
            // })
            ->orderBy(DB::raw('hide_week_id is not null'))
            ->orderBy(DB::raw('status = 2'))
            ->orderBy('cycle_name')
            ->orderBy('week_id')
            ->paginate(15);

        $responsible_list = [];

        foreach ($metric_action_list as $metric_action) {

            if (!json_decode($metric_action->responsible)) {
                $metric_action->responsible = [$metric_action->responsible];
                continue;
            }

            $metric_action->responsible = json_decode($metric_action->responsible);

            if (!is_array($metric_action->responsible)) {
                $metric_action->responsible = explode(",", $metric_action->responsible);
                continue;
            }

            foreach ($metric_action->responsible as $responsible) {
                if (isset($responsible->user_id)) {
                    $responsible_list[] = $responsible->user_id;
                }
            }
        }

        $bitrix_user_list = DB::table('BitrixUsers')
            ->select('id', 'email', 'user_id', 'name', 'last_name')
            ->whereIn('user_id', $responsible_list)
            ->get();

        $response['success'] = true;

        $response['data'] = [
            'bitrix_user_list' => $bitrix_user_list,
            'metric_lag_list' => $metric_lag_list,
            'metric_action_list' => $metric_action_list->items(),
            'metric_action_list_meta' => [
                'total' => $metric_action_list->total(),
                'current_page' => $metric_action_list->currentPage(),
                'last_page' => $metric_action_list->lastPage()
            ]
        ];

        return $response;
    }
}
