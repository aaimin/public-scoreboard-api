<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MetricAggregateIndexApiController extends Controller {

    function __invoke () {
        
        $collection = DB::table('pn_metric_aggregates as ma')
            ->select(
                'ma.*',
                'source_user.name as source_user_name',
                'target_user.name as target_user_name',
                'source_metric.name as source_metric_name',
                'target_metric.name as target_metric_name'
            )
            ->leftJoin('users as source_user', 'ma.source_user_id', 'source_user.id')
            ->leftJoin('users as target_user', 'ma.target_user_id', 'target_user.id')
            ->leftJoin('metrics as source_metric', function ($join) {
                $join->on('ma.cycle_name', 'source_metric.cycle_name')
                    ->on('ma.source_user_id', 'source_metric.user_id')
                    ->on('ma.source_ref_metric', 'source_metric.ref_metric');
            })
            ->leftJoin('metrics as target_metric', function ($join) {
                $join->on('ma.cycle_name', 'target_metric.cycle_name')
                    ->on('ma.target_user_id', 'target_metric.user_id')
                    ->on('ma.target_ref_metric', 'target_metric.ref_metric');
            })
            ->where('target_user_id', auth()->user()->id)
            ->paginate();

        $source_user_id_collection = $collection->pluck('source_user_id');

        $source_user_collection = DB::table('users')
            ->select('id', 'name', 'email')
            ->whereIn('id', $source_user_id_collection)
            ->get();
        
        $response['status'] = 'success';

        $response['data'] = [
            'metric_aggregate_list' => $collection->items(),
            'source_user_collection' => $source_user_collection
        ];

        $response['meta'] = [
            'metric_aggregate_list' => [
                'current_page' => $collection->currentPage(),
                'last_page' => $collection->lastPage(),
                'total' => $collection->total()
            ]
        ];

        return $response;
    }
}
