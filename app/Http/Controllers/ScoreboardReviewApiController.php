<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\ScoreboardReviewApproved;
use App\Mail\ScoreboardReviewRejected;
use App\Mail\ScoreboardReviewSubmitted;
use App\Models\ScoreboardReview;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Validator;

class ScoreboardReviewApiController extends Controller {

    public function index(Request $request) {

        $user = auth()->user();

        $scoreboard_review_list = DB::table('pn_scoreboard_review as sr')
            ->select(
                'sr.*',
                'requested_by.name as requested_by_name',
                'requested_by.email as requested_by_email',
                'reviewed_by.name as reviewed_by_name',
                'reviewed_by.email as reviewed_by_email'
            )
            ->leftJoin('users as requested_by', 'sr.requested_by', '=', 'requested_by.id')
            ->leftJoin('users as reviewed_by', 'sr.reviewed_by', '=', 'reviewed_by.id')
            ->where('requested_by', $user->id)
            ->orWhere('reviewed_by', $user->id)
            ->orderBy(DB::raw('status is null'), 'desc')
            ->paginate();

        $response['data'] = [
            'scoreboard_review_list' => $scoreboard_review_list->items()
        ];

        $response['meta'] = [
            'scoreboard_review_list' => [
                'current_page' => $scoreboard_review_list->currentPage(),
                'last_page' => $scoreboard_review_list->lastPage(),
                'total' => $scoreboard_review_list->total()
            ]
        ];

        $response['status'] = 'success';

        return $response;
    }

    public function store(Request $request) {

        $user = auth()->user();

        $validator = Validator::make($request->all(), [
            'cycle_name' => 'required',
            'requested_by' => 'required',
            'reviewed_by' => 'required'
        ])->validate();

        $newRecord = $request->only([
            'cycle_name',
            'requested_by',
            'reviewed_by'
        ]);

        $newRecord['requested_at'] = date('Y-m-d H:i:s');

        $create = ScoreboardReview::create($newRecord);

        $reviewer = DB::table('users')
            ->find($request->input('reviewed_by'));

        if (!$reviewer) {
            return ['status' => 'error'];
        }

        $sender = $user->name;
        $cycle_name = $request->input('cycle_name');

        $subject = "$sender has submmitted scoreboard review for $cycle_name";

        $url = 'https://pn.greenpacket.com/#/scoreboard-review';

        if (env('APP_ENV') == 'local') {
            $url = 'http://localhost:8080/#/scoreboard-review';
        }

        Mail::to($reviewer->email)->send(
            new ScoreboardReviewSubmitted([
                'subject' => $subject,
                'url' => $url,
                'sender' => $sender,
                'cycle_name' => $cycle_name
            ])
        );

        $response['status'] = 'success';

        return $response;
    }

    public function show(Request $request, $id) {
        
    }

    public function update(Request $request, $id) {
        //
    }

    public function destroy(Request $request, $id) {

        DB::table('pn_scoreboard_review')
            ->where('id', $id)
            ->whereNull('status')
            ->delete();

        $response['status'] = 'success';

        return $response;
    }

    public function approve(Request $request, $id) {

        $comment = $request->input('comment');

        DB::table('pn_scoreboard_review')
            ->where('id', $id)
            ->update([
                'comment' => $comment,
                'reviewed_at' => date('Y-m-d H:i:s'),
                'status' => 'approved'
            ]);

        $scoreboard_review = DB::table('pn_scoreboard_review')
            ->find($id);

        if (!$scoreboard_review) {
            return ['status' => 'error'];
        }

        $requester = DB::table('users')
            ->find($scoreboard_review->requested_by);

        if (!$requester) {
            return ['status' => 'error'];
        }

        $cycle_name = $scoreboard_review->cycle_name;
        $comment = $scoreboard_review->comment;

        $subject = "Your scoreboard review for $cycle_name has been approved.";

        $url = 'https://pn.greenpacket.com/#/scoreboard-review';

        if (env('APP_ENV') == 'local') {
            $url = 'http://localhost:8080/#/scoreboard-review';
        }

        Mail::to($requester->email)->send(
            new ScoreboardReviewApproved([
                'subject' => $subject,
                'url' => $url,
                'cycle_name' => $cycle_name,
                'comment' => $comment
            ])
        );

        $response['status'] = 'success';

        return $response;
    }

    public function reject(Request $request, $id) {

        $comment = $request->input('comment');

        DB::table('pn_scoreboard_review')
            ->where('id', $id)
            ->update([
                'comment' => $comment,
                'reviewed_at' => date('Y-m-d H:i:s'),
                'status' => 'rejected'
            ]);

        $scoreboard_review = DB::table('pn_scoreboard_review')
            ->find($id);

        if (!$scoreboard_review) {
            return ['status' => 'error'];
        }

        $requester = DB::table('users')
            ->find($scoreboard_review->requested_by);

        if (!$requester) {
            return ['status' => 'error'];
        }

        $cycle_name = $scoreboard_review->cycle_name;
        $comment = $scoreboard_review->comment;

        $subject = "Your scoreboard review for $cycle_name has been rejected.";

        $url = 'https://pn.greenpacket.com/#/scoreboard-review';

        if (env('APP_ENV') == 'local') {
            $url = 'http://localhost:8080/#/scoreboard-review';
        }

        Mail::to($requester->email)->send(
            new ScoreboardReviewRejected([
                'subject' => $subject,
                'url' => $url,
                'cycle_name' => $cycle_name,
                'comment' => $comment
            ])
        );

        $response['status'] = 'success';

        return $response;
    }
}
