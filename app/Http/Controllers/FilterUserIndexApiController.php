<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class FilterUserIndexApiController extends Controller {

    public function __invoke(Request $request) {

        $collection = DB::table('users')
            ->select(
                'users.id',
                'users.name',
                DB::raw('group_concat(distinct uug.user_group_id) user_group_id')
            )->leftJoin('pn_user_user_groups as uug', 'uug.user_id', 'users.id')
            ->groupBy('users.id')
            ->get();

        return ['data' => $collection];
    }
}
