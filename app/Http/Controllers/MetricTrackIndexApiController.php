<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MetricTrack;

class MetricTrackIndexApiController extends Controller {

    public function __invoke(Request $request) {

        $user_id = auth()->user()->id;
        $week_id = $request->query('week_id');

        $collection = MetricTrack::where('user_id', $user_id)
            ->where('week_id', $week_id)
            ->get();

        return [
            'collection' => $collection
        ];
    }
}
