<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Metric;
use App\Models\MetricTrack;
use App\Models\MetricAction;

class ScoreboardIndexApiController extends Controller {
    
    public function __invoke (Request $request) {

        $user_id = $request->query('user_id');
        $cycle_name = $request->query('cycle_name');
        $auth_user_id = auth()->user()->id;
        
        $max_week = $request->query('week_id');

        // Temporary Solution.

        if ($cycle_name == '2019-Q3') {
            $max_week = 39;
        }

        if ($cycle_name == '2019-Q4') {
            $max_week = 52;
        }

        $last_week = MetricTrack::where('user_id', $user_id)
            ->where('cycle_name', $cycle_name)
            ->max('week_id');

        if ($max_week && (!$last_week || $last_week > $max_week)) {
            $last_week = $max_week;
        }

        if (!$last_week) {
            $last_week = 1;
        }

        // $DateTime = new DateTime;
        // $DateTimeWeek = (int) $DateTime->format('W');
        // $quarter = (int) ceil($DateTimeWeek/13);

        $quarter = substr($cycle_name, 6);
        $quarterLastWeek = $quarter * 13;
        $quarterFirstWeek = $quarterLastWeek - 12;

        $metrics = Metric::with([
                'detail',
                'chart',
                'tracks'
            ])
            ->where('cycle_name', $cycle_name)
            ->where('user_id', $user_id)
            ->orderBy('ref_wig')
            ->orderBy('ref_lag')
            ->orderBy('ref_lead')
            ->orderBy('ref_act')
            ->orderBy('ref_qact')
            ->get();

        $user = DB::table('users')
            ->select('name', 'email')
            ->where('id', $user_id)
            ->first();

        // return $metrics;

        $tracksBase = [];

        for ($index = 0; $index < 13; $index++) {

            $tracksBase[] = [
                'week_id' => $quarterFirstWeek + $index,
                'track_value' => null,
                'target_value' => null
            ];
        }

        $tracks = $tracksBase;

        foreach($metrics as $index => $metric) {

            foreach($metric->tracks as $track) {

                $week_id = $track->week_id;
                $tracks_index = $week_id - $quarterFirstWeek;

                $tracks[$tracks_index] = $track;
            }

            unset($metric->tracks);
            $metric->tracks = $tracks;
            $tracks = $tracksBase;
        }

        $metric_actions = MetricAction::select('*')
            ->where('user_id', $user_id)
            ->where('cycle_name', $cycle_name)
            ->where('week_id', '<=', $last_week)
            ->whereNull('hide_week_id')
            ->get();

        foreach ($metric_actions as $metric_action) {

            if (!json_decode($metric_action->responsible)) {
                continue;
            }

            $metric_action->responsible = json_decode($metric_action->responsible);
        }

        // foreach ($metrics as $metric) {

        //     if ($metric['detail']) {
        //         $metric['detail']['data'] = json_decode($metric['detail']['data']);
        //         $metric['detail']['cellStyle'] = json_decode($metric['detail']['cellStyle']);
        //     }
        // }

        return response()->json([
            'metrics' => $metrics,
            'metric_actions' => $metric_actions,
            'user' => $user,
            'max_week' => $last_week
        ]);
    }
}
