<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\BitrixUser;

class BitrixUserIndexApiController extends Controller {

    public function __invoke(Request $request) {

        $query = $request->query('query');

        $status = 'success';

        $data = BitrixUser::select('id', 'email', 'user_id', 'name', 'last_name')
            ->where('email', 'like', "%${query}%")
            ->orWhere('name', 'like', "%${query}%")
            ->orWhere('last_name', 'like', "%${query}%")
            ->limit(10)
            ->get();

        return ['status' => $status, 'data' => $data];
    }
}
