<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\MetricAggregate;
use App\Models\MetricTrack;

class MetricAggregateValueIndexApiController extends Controller {

    public function __invoke(Request $request) {

        $user_id = auth()->user()->id;
        $week_id = $request->query('week_id');

        $collection = MetricAggregate::where('target_user_id', $user_id)
            ->get();

        $grouped_collection = $collection->groupBy('target_ref_metric');

        $results = [];

        foreach ($grouped_collection as $collection) {
            
            $value = MetricTrack::select(
                DB::raw('sum(track_value) as value')
            );

            foreach ($collection as $record) {

                $value = $value->orWhere(function ($query) use ($record, $week_id) {
                    $query->where('user_id', $record->source_user_id)
                        ->where('ref_metric', $record->source_ref_metric)
                        ->where('week_id', $week_id);
                });
            }

            $value = $value->first()->value;

            $results[] = [
                'user_id' => $record->target_user_id,
                'ref_metric' => $record->target_ref_metric,
                'week_id' => $week_id,
                'value' => $value
            ];
        }

        return [
            'status' => 'success',
            'data' => $results
        ];
    }
}
