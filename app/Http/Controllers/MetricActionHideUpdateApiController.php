<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\MetricAction;
use Illuminate\Http\Request;

class MetricActionHideUpdateApiController extends Controller {

    public function __invoke(Request $request) {

        $user_id = auth()->user()->id;

        $id = $request->input('id');
        $week_id = $request->input('week_id');
        $ref_metric = $request->input('ref_metric');

        MetricAction::where('id', $id)
            ->where('user_id', $user_id)
            ->where('ref_metric', $ref_metric)
            ->update([
                'hide_week_id' => $week_id,
                'hid_at' => date('Y-m-d H:i:s')
            ]);

        return [
            'status' => 'success'
        ];
    }
}
