<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use DateTime;
use App\Http\Controllers\Controller;
use App\Models\MetricQuery;

class MetricQueryValueIndexApiController extends Controller {

    public function __invoke(Request $request) {

        $user_id = auth()->user()->id;
        $week_id = $request->query('week_id');

        $datetime_year = new DateTime();
        $year = $datetime_year->format('Y');

        $datetime = new DateTime();
        $datetime->setISODate($year, $week_id);
        $week_start = $datetime->format('Y-m-d');
        $datetime->modify('+6 days');
        $week_end = $datetime->format('Y-m-d');

        $collection = MetricQuery::with('value_query')
            ->where('user_id', $user_id)
            ->get();

        $results = [];

        foreach ($collection as $record) {

            $query = $record
                ->value_query
                ->query;

            $query = str_replace('WEEKDATESTART', $week_start, $query);
            $query = str_replace('WEEKDATEEND', $week_end, $query);

            $value = DB::select($query);

            if ($value) {
                $value = $value[0];
            }

            $results[] = [
                'user_id' => $record->user_id,
                'ref_metric' => $record->ref_metric,
                'week_id' => $week_id,
                'value' => $value->value
            ];
        }

        return [
            'status' => 'success',
            'data' => $results
        ];
    }
}
