<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FeedbackResponse;

class FeedbackAnswerStoreApiController extends Controller {

    public function __invoke(Request $request) {

        $user = auth()->user();

        $multiple_feedback_answer =$request->input('multiple_feedback_answer');

        foreach ($multiple_feedback_answer as $index => $element) {

            $element['user_id'] = $user->id;
            $element['created_at'] = date('Y-m-d H:i:s');
            $element['updated_at'] = date('Y-m-d H:i:s');

            $multiple_feedback_answer[$index] = $element;
        }

        FeedbackResponse::insert($multiple_feedback_answer);

        return [
            'status' => 'success'
        ];
    }
}
