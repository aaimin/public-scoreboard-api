<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ScoreboardCheckin;

class MetricWithRelationshipsIndexApiController extends Controller {

    public function __invoke (Request $request) {

        $user_id = auth()->user()->id;
        $week_id = $request->query('week_id');
        $cycle_name = $request->input('cycle_name');

        // $DateTimeCurrent = new DateTime('2019-10-01');
        // $DateTimeCurrentWeek = $DateTimeCurrent->format('W');

        $lastScoreboardCheckIn = ScoreboardCheckin::where('user_id', $user_id)
            ->orderBy('week_id', 'desc')
            ->first();

        $metrics = DB::table('metrics as m')
            ->select(
                'm.*',
                'md.*',
                'm.user_id',
                'm.ref_metric',
                'm.cycle_name',
                'm.target_value as metric_target_value',
                'mc.chart_type',
                'mc.chart_stacked',
                'mc.chart_horizontal',
                'mc.data as mc_data',
                'mt.target_value',
                'mt.track_value',
                'm.id',
                DB::raw('if(count(ma.id) > 0, 1, 0) as is_aggregate')
            )
            ->leftJoin('pn_metric_details as md', function ($join) {
                $join->on('m.user_id', 'md.user_id')
                    ->on('m.cycle_name', 'md.cycle_name')
                    ->on('m.ref_metric', 'md.ref_metric');
            })
            ->leftJoin('pn_metric_charts as mc', function ($join) {
                $join->on('m.user_id', 'mc.user_id')
                    ->on('m.cycle_name', 'mc.cycle_name')
                    ->on('m.ref_metric', 'mc.ref_metric');
            })
            ->leftJoin('metric_tracks as mt', function ($join) use ($week_id) {
                $join->on('m.user_id', 'mt.user_id')
                    ->on('m.cycle_name', 'mt.cycle_name')
                    ->on('m.ref_metric', 'mt.ref_metric')
                    ->where('mt.week_id', $week_id);
            })
            ->leftJoin('pn_metric_aggregates as ma', function ($join) {
                $join->on('m.cycle_name', 'ma.cycle_name')
                    ->on('m.user_id', 'ma.target_user_id')
                    ->on('m.ref_metric', 'ma.target_ref_metric');
            })
            ->where('m.user_id', $user_id)
            ->where('m.cycle_name', $cycle_name)
            ->orderBy('ref_wig')
            ->orderBy('ref_lag')
            ->orderBy('ref_lead')
            ->orderBy('ref_act')
            ->orderBy('ref_qact')
            ->groupBy('m.ref_metric')
            ->get();

        foreach ($metrics as $metric) {

            if ($metric->data) {
                $metric->detail = [
                    'data' => $metric->data,
                    // 'headers' => $metric->headers,
                    // 'data' => json_decode($metric->data),
                    // 'cellStyle' => json_decode($metric->cellStyle)
                ];
            }

            if ($metric->mc_data) {
                $metric->chart = [
                    'chart_type' => $metric->chart_type,
                    'chart_stacked' => $metric->chart_stacked,
                    'data' => json_decode($metric->mc_data),
                ];
            }

            if (is_null($metric->target_value)) {
                $metric->target_value = $metric->metric_target_value;
            }

            unset($metric->data);
            unset($metric->headers);
            unset($metric->cellStyle);

            unset($metric->chart_type);
            unset($metric->chart_stacked);
            unset($metric->mc_data);
        }

        return [
            'metrics' => $metrics,
            'last_scoreboard_checkin' => $lastScoreboardCheckIn
        ];
    }
}
