<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use DateTime;
use App\Http\Controllers\Controller;
use App\Models\MetricQuery;

class MetricQueryDetailIndexApiController extends Controller {

    public function __invoke(Request $request) {

        $user_id = auth()->user()->id;
        $week_id = $request->query('week_id');
        $ref_metric = $request->query('ref_metric');

        $datetime_year = new DateTime();
        $year = $datetime_year->format('Y');

        $datetime = new DateTime();
        $datetime->setISODate($year, $week_id);
        $week_start = $datetime->format('Y-m-d');
        $datetime->modify('+6 days');
        $week_end = $datetime->format('Y-m-d');

        $record = MetricQuery::with('detail_query')
            ->where('user_id', $user_id)
            ->where('ref_metric', $ref_metric)
            ->first();

        $detail = [];

        if ($record && $record->detail_query) {

            $query = $record->detail_query->query;
            $query = str_replace('WEEKDATESTART', $week_start, $query);
            $query = str_replace('WEEKDATEEND', $week_end, $query);
    
            $detail = DB::select($query);
        }


        return [
            'status' => 'success',
            'data' => [
                'user_id' => $user_id,
                'ref_metric' => $ref_metric,
                'week_id' => $week_id,
                'detail' => $detail
            ]
        ];
    }
}
