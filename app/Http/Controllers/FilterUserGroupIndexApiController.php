<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class FilterUserGroupIndexApiController extends Controller {

    public function __invoke(Request $request) {

        $collection = DB::table('pn_user_groups')
            ->select('id', 'business_unit', 'name')
            ->get();

        return ['data' => $collection];
    }
}
