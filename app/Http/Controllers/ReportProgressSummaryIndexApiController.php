<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use DateTime;

class ReportProgressSummaryIndexApiController extends Controller {

    public function __invoke(Request $request) {

        $user = auth()->user();

        $cycle_name = '2020-Q1';
        $selected_week = $request->query('week');
        $selected_user_group_id = $request->query('user_group_id');

        if (!$selected_week) {

            $date_time = new DateTime;
            $date_time_day = (int) $date_time->format('N');
            $date_time_week = (int) $date_time->format('W');
            
            if ($date_time_day <= 4) {
                $date_time_week = $date_time_week - 1;
            }

            $selected_week = $date_time_week;
        }

        $max_week = $selected_week;
        $min_week = $max_week - 4;

        $user_groups = DB::table('pn_user_groups as ug')
            ->select('ug.*', 'uug.is_admin')
            ->join('pn_user_user_groups as uug', 'ug.id', 'uug.user_group_id')
            ->where('uug.user_id', $user->id)
            ->orderBy('ug.business_unit')
            ->orderBy('ug.name')
            ->get();

        if (!$selected_user_group_id) {

            $item_id = null;

            foreach ($user_groups as $item) {
                
                $item_id = $item->id;

                if ($item->business_unit != $item->name) {
                    break;
                }
            }

            $selected_user_group_id = $item_id;
        }

        $selected_user_group = $user_groups
            ->firstWhere('id', $selected_user_group_id);

        $selected_is_business_unit =
            $selected_user_group->business_unit
                == $selected_user_group->name;

        $selected_is_admin = $selected_user_group->is_admin;

        if (!$selected_is_business_unit || $selected_is_admin) {

            $users = DB::table('users')
                ->select('users.id', 'users.name')
                ->join('pn_user_user_groups as uug', 'uug.user_id', 'users.id')
                ->leftJoin('pn_scoreboard_checkin as sc1', function ($join) use ($cycle_name, $max_week) {
                    $join->on('users.id', '=', 'sc1.user_id')
                        ->where('sc1.cycle_name', $cycle_name)
                        ->where('sc1.week_id', $max_week);
                })
                ->leftJoin('pn_scoreboard_checkin as sc2', function ($join) use ($cycle_name, $max_week) {
                    $join->on('users.id', '=', 'sc2.user_id')
                        ->where('sc2.cycle_name', $cycle_name)
                        ->where('sc2.week_id', $max_week - 1);
                })
                ->where('uug.user_group_id', $selected_user_group_id)
                ->where('uug.is_admin', 0)
                ->where(function ($query) {
                    $query->whereNotNull('sc1.id')
                        ->orWhereNotNull('sc2.id');
                })
                ->orderBy('users.name')
                ->get();
        }

        else {

            $users = DB::table('users')
                ->select('users.id', 'users.name')
                ->leftJoin('pn_scoreboard_checkin as sc1', function ($join) use ($cycle_name, $max_week) {
                    $join->on('users.id', '=', 'sc1.user_id')
                        ->where('sc1.cycle_name', $cycle_name)
                        ->where('sc1.week_id', $max_week);
                })
                ->leftJoin('pn_scoreboard_checkin as sc2', function ($join) use ($cycle_name, $max_week) {
                    $join->on('users.id', '=', 'sc2.user_id')
                        ->where('sc2.cycle_name', $cycle_name)
                        ->where('sc2.week_id', $max_week - 1);
                })
                ->where(function ($query) {
                    $query->whereNotNull('sc1.id')
                        ->orWhereNotNull('sc2.id');
                })
                ->where('users.id', $user->id)
                ->get();
        }

        $user_ids = $users->pluck('id');

        $last_update_date = DB::table('pn_user_report')
            ->max('created_at');

        $user_reports = DB::table('pn_user_report')
            ->whereIn('user_id', $user_ids)
            ->where('cycle_name', $cycle_name)
            ->where('week_id', '>=', $min_week)
            ->where('week_id', '<=', $max_week)
            ->get();

        $grouped_user_reports = $user_reports
            ->groupBy(['report_key', 'user_id'])
            ->toArray();

        $report_keys = [
            'WIG2LAG',
            'WIG2LEAD',
            'WIG2ACT',
            'CAPA_OPEN',
            'CAPA_COMPLETE',
            'RECOGNITION'
        ];

        foreach ($report_keys as $report_key) {

            $user_id_grouped_user_reports = Arr::get(
                $grouped_user_reports,
                $report_key,
                []
            );

            foreach ($users as $index => $user) {
                
                $values = array_fill(0, 5, 0);

                $user_user_reports = Arr::get(
                    $user_id_grouped_user_reports,
                    $user->id,
                    []
                );

                foreach ($user_user_reports as $record) {

                    if (!$record || !$record->report_value) {
                        continue;
                    }

                    $index = $record->week_id - $min_week;
                    $values[$index] = $record->report_value;
                }

                $user->$report_key = $values;
            }
        }

        return [
            'collection' => $users,
            'user_groups' => $user_groups,
            'user_group_id' => $selected_user_group_id,
            'last_update_date' => $last_update_date
        ];
    }
}
