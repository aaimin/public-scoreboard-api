<?php

namespace App\Http\Controllers;

use App\Helpers\Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ReportScoreboardSubmissionsIndexApiController extends Controller {

    public function __invoke(Request $request) {

        $user_group_id = $request->query('user_group_id');

        $current_week = Helpers::getCurrentWeek();
        $week_id = $request->query('week_id', $current_week);

        $user = auth()->user();
        $user_id = $user->id;

        $user_group_list = DB::table('pn_user_groups as ug')
            ->select(
                DB::raw("if(ug_all.business_unit = ug_all.name, 'BUCU', 'Group') type"),
                'ug.id as bucu_id',
                'ug_all.id as group_id',
                'ug_all.business_unit',
                'ug_all.name'
            )
            ->leftJoin('pn_user_groups as ug_all', 'ug.business_unit', 'ug_all.business_unit')
            ->join('pn_user_user_groups as uug', 'ug.id', 'uug.user_group_id')
            ->whereColumn('ug.business_unit', 'ug.name')
            ->where('user_id', $user_id)
            ->orderBy('type')
            ->orderBy('ug.business_unit')
            ->get();

        if ($user_group_id) {
        
            $user_group_type = $user_group_list
                ->firstWhere('group_id', $user_group_id)
                ->type;

            if ($user_group_type == 'BUCU') {

                $user_group_id_list = $user_group_list
                    ->where('bucu_id', $user_group_id)
                    ->pluck('group_id')
                    ->all();
            }

            else {

                $user_group_id_list = [$user_group_id];
            }
        }

        else {

            $user_group_id_list = $user_group_list
                ->pluck('group_id')
                ->all();
        }

        if ($user_group_id) {

            $report_collection = DB::table('users')
                ->distinct()
                ->select(
                    'users.id',
                    'users.name',
                    DB::raw('if(sc.updated_at, 1, 0) checked_in'),
                    DB::raw("if(sc.updated_at < str_to_date(concat(cw.year, cw.week, 0, 0), '%Y %u %w %H'), 1, 0) on_time"),
                    DB::raw('1 target')
                )
                ->crossJoin('pn_calendar_weeks as cw')
                ->leftJoin('pn_scoreboard_checkin as sc', function ($join) {
                    $join->on('users.id', '=', 'sc.user_id')
                        ->on('cw.week', '=', 'sc.week_id');
                })
                ->leftJoin('pn_user_user_groups as uug', 'users.id', 'uug.user_id')
                ->leftJoin('pn_user_groups as ug', 'uug.user_group_id', 'ug.id')
                ->where('users.is_active', 1)
                ->where('uug.is_admin', 0)
                ->whereIn('ug.id', $user_group_id_list)
                ->where('cw.year', '2020')
                ->where('cw.week', $week_id)
                ->orderBy('name')
                ->get();

            $report_aggregate = DB::query()
                ->select(
                    DB::raw("'Aggregate' name"),
                    DB::raw('sum(checked_in) checked_in'),
                    DB::raw('sum(on_time) on_time'),
                    DB::raw('count(1) target'),
                    DB::raw('round(sum(checked_in) / count(1) * 100, 0) average_checked_in'),
                    DB::raw('round(sum(on_time) / count(1) * 100, 0) average_on_time')
                )
                ->fromSub(function ($query) use ($week_id, $user_group_id) {
                    
                    $query
                        ->distinct()
                        ->from('users')
                        ->select(
                            'users.id',
                            DB::raw('if(sc.updated_at, 1, 0) checked_in'),
                            DB::raw("if(sc.updated_at < str_to_date(concat(cw.year, cw.week, 0, 0), '%Y%u%w%H'), 1, 0) on_time")
                        )
                        ->crossJoin('pn_calendar_weeks as cw')
                        ->leftJoin('pn_scoreboard_checkin as sc', function ($join) {
                            $join->on('users.id', '=', 'sc.user_id')
                                ->on('cw.week', '=', 'sc.week_id');
                        })
                        ->leftJoin('pn_user_user_groups as uug', 'users.id', 'uug.user_id')
                        ->leftJoin('pn_user_groups as ug', 'uug.user_group_id', 'ug.id')
                        ->where('users.is_active', 1)
                        ->where('uug.is_admin', 0)
                        ->where('cw.week', $week_id)
                        ->where('ug.id', $user_group_id);

                }, 'collection')

                ->first();
        }

        else {

            $report_collection = DB::table('users')
                ->select(
                    'ug.id',
                    DB::raw("concat(ug.business_unit, ' - ', ug.name) name"),
                    DB::raw('sum(if(sc.updated_at, 1, 0)) checked_in'),
                    DB::raw("sum(if(sc.updated_at < str_to_date(concat(cw.year, cw.week, 0, 0), '%Y %u %w %H'), 1, 0)) on_time"),
                    DB::raw('count(1) target')
                )
                ->crossJoin('pn_calendar_weeks as cw')
                ->leftJoin('pn_scoreboard_checkin as sc', function ($join) {
                    $join->on('users.id', '=', 'sc.user_id')
                        ->on('cw.week', '=', 'sc.week_id');
                })
                ->leftJoin('pn_user_user_groups as uug', 'users.id', 'uug.user_id')
                ->leftJoin('pn_user_groups as ug', 'uug.user_group_id', 'ug.id')
                ->where('users.is_active', 1)
                ->where('uug.is_admin', '!=', 1)
                ->whereIn('ug.id', $user_group_id_list)
                ->where('cw.week', $week_id)
                ->groupBy('id', 'name')
                ->orderBy(DB::raw('ug.business_unit = ug.name'), 'desc')
                ->orderBy('ug.business_unit')
                ->orderBy('ug.name')
                ->get();

                $report_aggregate = [];
        }

        $report_collection->map(function ($item) {

            $item->average_checked_in = round($item->checked_in / $item->target * 100, 0);
            $item->average_on_time = round($item->on_time / $item->target * 100, 0);
        });

        return [            
            'data' => [
                'report_collection' => $report_collection,
                'report_aggregate' => $report_aggregate,
                'user_group_list' => $user_group_list,
                'week_id' => $week_id
            ],
            'success' => true,
        ];
    }
}
