<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserIndexApiController extends Controller {

    public function __invoke(Request $request) {

        $search = $request->query('search');

        $user_list = DB::table('users')
            ->select('id', 'name', 'email');
        
        if ($search) {

            $user_list = $user_list
                ->where('name', 'like', "%$search%")
                ->orWhere('full_name', 'like', "%$search%")
                ->orWhere('email', 'like', "%$search%");
        }
        
        $user_list = $user_list
            ->orderBy('name')
            ->orderBy('email')
            ->paginate();

        $response['data'] = [
            'user_list' => $user_list->items()
        ];

        $response['meta'] = [
            'user_list' => [
                'current_page' => $user_list->currentPage(),
                'last_page' => $user_list->lastPage(),
                'total' => $user_list->total()
            ]
        ];

        $response['status'] = 'success';

        return $response;
    }
}
