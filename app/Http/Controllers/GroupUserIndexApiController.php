<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

use App\User;
use App\Models\UserGroup;
use App\Models\UserDetail;

class GroupUserIndexApiController extends Controller {
    
    public function __invoke(Request $request) {

        $user_id = auth()->user()->id;
        $user_groups = auth()->user()->user_groups;

        if (!$user_groups) {
            return [
                'users' => [],
                'user_groups' => []
            ];
        }

        $business_units = Arr::pluck($user_groups, 'business_unit');

        $users = User::where('is_active', 1)
            ->whereHas('user_groups', function (Builder $query) use ($business_units) {
                $query->whereIn('pn_user_groups.business_unit', $business_units);
            })
            ->get();

        $user_groups = UserGroup::with([
                'pivot_users' => function ($query) {
                    $query->where('is_admin', false);
                }
            ])
            ->whereIn('business_unit', $business_units)
            ->get();

        return [
            'users' => $users,
            'user_groups' => $user_groups
        ];
    }
}
