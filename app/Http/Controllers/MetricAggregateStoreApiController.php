<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MetricAggregateStoreApiController extends Controller {

    public function __invoke(Request $request) {

        $new_item = $request->only([
            'name',
            'cycle_name',
            'source_ref_metric',
            'source_user_id',
            'target_ref_metric',
        ]);
        
        $new_item['target_user_id'] = auth()->user()->id;
        $new_item['created_at'] = date('Y-m-d H:i:s');
        $new_item['updated_at'] = date('Y-m-d H:i:s');

        DB::table('pn_metric_aggregates')
            ->insert($new_item);

        return ['status' => 'success'];
    }
}
