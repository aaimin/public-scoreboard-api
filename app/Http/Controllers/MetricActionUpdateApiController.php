<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Actions\BitrixTaskStoreAction;
use App\Mail\BitrixTaskCreated;
use App\Models\MetricAction;

class MetricActionUpdateApiController extends Controller {

    public function __invoke(Request $request) {

        // $BitrixTaskStoreAction = new BitrixTaskStoreAction();

        $user = auth()->user();
        $user_id = $user->id;
        $user_name = $user->name;
        $week_id = $request->input('week_id');
        $cycle_name = $request->input('cycle_name');

        $keys = [
            'ref_metric',
            'type',
            'issue',
            'capa',
            'accountable',
            'notify_responsible',
            'deadline'
        ];

        $input = $request->input('metric_action', []);

        $id = Arr::get($input, 'id');

        $validation = Validator::make($input, []);

        if ($validation->fails()) {
            http_response_code(422);
            die(json_encode($validation->errors()));
        }

        $new_record = [];

        foreach($keys as $key) {
            $new_record[$key] = Arr::get($input, $key);
        }

        $new_record['responsible'] = json_encode(
            Arr::get($input, 'responsible', [])
        );

        $date = date('Y-m-d H:i:s');

        $new_record['user_id'] = $user_id;
        $new_record['cycle_name'] = $cycle_name;
        $new_record['week_id'] = $week_id;
        $new_record['created_at'] = $date;
        $new_record['updated_at'] = $date;

        if(strtolower($new_record['type']) == 'recognition') {
            $new_record['status'] = 2;
            $new_record['complete_week_id'] = $week_id;
            $new_record['completed_at'] = $date;
        }

        $metricAction = MetricAction::updateOrCreate([
            'id' => $id,
            'user_id' => $user_id,
            'cycle_name' => $cycle_name,
            'week_id' => $week_id
        ], $new_record);

        $responsibles = json_decode($new_record['responsible'], true);

        foreach ($responsibles as $responsible) {

            if (!is_array($responsible)) {
                continue;
            }

            $notify_responsible = Arr::get($new_record, 'notify_responsible');
            
            if ($notify_responsible) {

                $email = Arr::get($responsible, 'email');
                $name = Arr::get($responsible, 'name');
                $last_name = Arr::get($responsible, 'last_name');

                $title = Arr::get($new_record, 'capa');
                $description = Arr::get($new_record, 'issue');
                $deadline = Arr::get($new_record, 'deadline');

                if (!$name) {
                    $name = $email;
                }

                // $BitrixTaskStoreAction->execute([
                //     'title' => $title,
                //     'description' => $description,
                //     'responsible_id' => Arr::get($responsible, 'user_id'),
                //     'deadline' => $deadline,
                //     'tags' => Arr::get($new_record, 'type')
                // ]);

                Mail::to($email)->send(new BitrixTaskCreated([
                    'task' => $title,
                    'description' => $description,
                    'deadline' => $deadline,
                    'assigner' => $user_name,
                    'responsible' => "$name $last_name"
                ]));
            }
        }
        
        return [
            'data' => [
                'id' => $metricAction->id
            ],
            'status' => 'success'
        ];
    }
}
