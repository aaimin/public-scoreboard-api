<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\MetricAggregate;
use App\Models\MetricTrack;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class MetricAggregateDetailIndexApiController extends Controller {

    public function __invoke(Request $request) {

        $user_id = auth()->user()->id;
        $week_id = $request->query('week_id');
        $ref_metric = $request->query('ref_metric');

        $collection = MetricAggregate::where('target_user_id', $user_id)
            ->where('target_ref_metric', $ref_metric)
            ->get();

        $detail = MetricTrack::with('metric', 'user');

        foreach ($collection as $record) {

            $detail = $detail->orWhere(function ($query) use ($record, $week_id) {
                $query->where('user_id', $record->source_user_id)
                    ->where('ref_metric', $record->source_ref_metric)
                    ->where('week_id', $week_id);
            });
        }

        $detail = $detail->get();

        $transformed = [];

        foreach ($detail as $record) {

            $transformed[] = [
                'User' => Arr::get($record, 'user.name'),
                'Metric' => Arr::get($record, 'metric.name'),
                'Week' => Arr::get($record, 'week_id'),
                'Value' => Arr::get($record, 'track_value'),
                'Target' => Arr::get($record, 'target_value')
            ];
        }

        return [
            'status' => 'success',
            'data' => [
                'detail' => $transformed
            ]
        ];
    }
}
