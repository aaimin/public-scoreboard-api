<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MetricIndexApiController extends Controller {

    public function __invoke(Request $request) {

        $user_id = $request->query('user_id');
        $cycle_name = $request->query('cycle_name');

        $collection = DB::table('metrics')
            ->where('user_id', $user_id)
            ->where('cycle_name', $cycle_name)
            ->get();

        $data['collection'] = $collection;
        
        $response['data'] = $data;
        $response['status'] = 'success';

        return $response;
    }
}
