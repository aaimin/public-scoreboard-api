<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Mail\ScoreboardCheckedIn;
use App\Models\ScoreboardCheckin;

class ScoreboardCheckinMailUpdateApiController extends Controller {

    public function __invoke(Request $request) {

        $user = auth()->user();
        $week_id = $request->input('week_id');
        $cycle_name = $request->input('cycle_name');
        
        $item = ScoreboardCheckin::where([
            ['mail_sent', 0],
            ['user_id', $user->id],
            ['week_id', $week_id],
            ['cycle_name', $cycle_name]
        ])->first();

        if (!$item) {
            return [
                'status' => 'error'
            ];
        }

        $emails = $item->only([
            'notify_email_1',
            'notify_email_2',
            'notify_email_3'
        ]);

        foreach ($emails as $email) {

            if (!$email) {
                continue;
            }

            $subject = "{$user->name} has checked in W{$week_id} scoreboard";

            $url = 'https://pn.greenpacket.com/#/scoreboard';

            if (env('APP_ENV') == 'local') {
                $url = 'http://localhost:8080/#/scoreboard';
            }

            $url .= "?user_id={$user->id}";

            Mail::to($email)->send(
                new ScoreboardCheckedIn([
                    'subject' => $subject,
                    'url' => $url,
                    'sender' => $user->name,
                    'week_id' => $week_id
                ])
            );
        }

        $item->mail_sent = 1;
        $item->save();

        return [
            'status' => 'success',
        ];
    }
}
