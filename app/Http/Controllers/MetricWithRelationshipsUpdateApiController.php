<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Mail\ScoreboardCheckedIn;
use App\Models\ScoreboardCheckin;
use App\Jobs\UpdateMetric;

class MetricWithRelationshipsUpdateApiController extends Controller {

    public function __invoke (Request $request) {

        // $input = $request->all();

        // dispatch(new UpdateMetric($input));
        // dispatch(new CreateMetricArchive($input));
        // dispatch(new UpdateMetricTrack($input));
        // dispatch(new CreateMetricTrackArchive($input));
        // dispatch(new UpdateMetricDetail($input));
        // dispatch(new CreateMetricDetailArchive($input));
        // dispatch(new UpdateMetricChart($input));
        // dispatch(new CreateMetricChartArchive($input));

        $checkin = $request->input('checkin', false);

        $this->storeMetrics($request);
        $this->storeMetricTracks($request);
        $this->storeMetricDetails($request);
        $this->storeMetricCharts($request);

        if ($checkin) {
            $this->storeScoreboardCheckin($request);
            $this->storeUserReports($request);
        }
    }

    public function storeMetrics (Request $request) {

        $start_week_of_2019_quarters = [1, 14, 27, 40];

        $user_id = auth()->user()->id;
        $week_id = $request->input('week_id');
        $cycle_name = $request->input('cycle_name');
        
        // $count_metrics = DB::table('metrics')
        //     ->where('user_id', $user_id)
        //     ->count();

        // if (!in_array($week_id, $start_week_of_2019_quarters) && $count_metrics) {
        //     return;
        // }

        $keys = [
            'ref_metric',
            'ref_wig',
            'ref_lag',
            'ref_lead',
            'ref_act',
            'ref_qact',
            'name',
            'type_id',
            'type_code',
            'target_unit',
            'target_value',
            'is_reverse_progress',
            'is_query',
            'is_aggregate'
        ];

        $input = $request->input('metrics', []);

        $validation = Validator::make($input, [
            // '*.ref_metric' => 'required',
            // '*.name' => 'required',
            // '*.type_id' => 'required',
            // '*.type_code' => 'required'
        ]);
        
        if ($validation->fails()) {
            http_response_code(422);
            die(json_encode($validation->errors()));
        }

        $data = [];

        foreach ($input as $record) {

            $new_record = [];

            foreach ($keys as $key) {

                $new_record[$key] = array_key_exists($key, $record)
                    ? $record[$key]
                    : null;
            }

            $new_record['user_id'] = $user_id;
            $new_record['cycle_name'] = $cycle_name;
            $new_record['created_at'] = date('Y-m-d H:i:s');
            $new_record['updated_at'] = date('Y-m-d H:i:s');

            $data[] = $new_record;
        }

        DB::table('metrics')
            ->where('user_id', $user_id)
            ->where('cycle_name', $cycle_name)
            ->delete();

        DB::table('metrics')
            ->insert($data);

        DB::table('archive_metrics')
            ->insert($data);
    }

    public function storeMetricTracks (Request $request) {

        $user_id = auth()->user()->id;
        $week_id = $request->input('week_id');
        $cycle_name = $request->input('cycle_name');

        $keys = [
            'ref_metric',
            'track_value',
            'target_value'
        ];

        $input = $request->input('metrics');

        $data = [];

        foreach ($input as $record) {

            $new_record = [];

            if ($record['type_code'] == 'wig') {
                continue;
            }

            foreach ($keys as $key) {
                
                $new_record[$key] = array_key_exists($key, $record)
                    ? $record[$key]
                    : null;
            }

            $new_record['user_id'] = $user_id;
            $new_record['cycle_name'] = $cycle_name;
            $new_record['week_id'] = $week_id;
            $new_record['created_at'] = date('Y-m-d H:i:s');
            $new_record['updated_at'] = date('Y-m-d H:i:s');

            $data[] = $new_record;
        }

        DB::table('metric_tracks')
            ->where('user_id', $user_id)
            ->where('cycle_name', $cycle_name)
            ->where('week_id', $week_id)
            ->delete();

        DB::table('metric_tracks')
            ->insert($data);

        DB::table('archive_metric_tracks')
            ->insert($data);
    }

    public function storeMetricDetails (Request $request) {

        $user_id = auth()->user()->id;
        $week_id = $request->input('week_id');
        $cycle_name = $request->input('cycle_name');
            
        $keys = [
            'ref_metric',
            'data',
            // 'headers',
            // 'cellStyle'
        ];

        $input = $request->input('metrics', []);

        $details = [];

        foreach ($input as $metric) {

            $detail = [];

            if (!array_key_exists('detail', $metric)) {
                continue;
            }

            $detail['ref_metric'] = $metric['ref_metric'];
            $detail['data'] = $metric['detail']['data'];
            // $detail['headers'] = $metric['detail']['headers'];
            // $detail['cellStyle'] = json_encode($metric['detail']['cellStyle']);

            $details[] = $detail;
        }

        $input = $details;

        $validation = Validator::make($input, [
            '*.data.*' => 'required',
            // '*.headers' => 'required'
        ]);

        if ($validation->fails()) {
            http_response_code(422);
            die(json_encode($validation->errors()));
        }

        $data = [];

        foreach ($input as $record) {

            $new_record = [];

            foreach ($keys as $key) {
                
                $new_record[$key] = array_key_exists($key, $record)
                    ? $record[$key]
                    : null;
            }

            $new_record['user_id'] = $user_id;
            $new_record['cycle_name'] = $cycle_name;
            $new_record['created_at'] = date('Y-m-d H:i:s');
            $new_record['updated_at'] = date('Y-m-d H:i:s');

            $data[] = $new_record;
        }

        DB::table('pn_metric_details')
            ->where('user_id', $user_id)
            ->where('cycle_name', $cycle_name)
            ->delete();

        DB::table('pn_metric_details')
            ->insert($data);

        DB::table('pn_archive_metric_details')
            ->insert($data);
    }

    public function storeMetricCharts (Request $request) {

        $user_id = auth()->user()->id;
        $week_id = $request->input('week_id');
        $cycle_name = $request->input('cycle_name');

        $keys = [
            'ref_metric',
            'chart_type',
            'chart_stacked',
            'chart_horizontal',
            'data',
        ];

        $input = $request->input('metrics', []);

        $charts = [];

        foreach ($input as $metric) {

            $chart = [];

            if (!array_key_exists('chart', $metric)) {
                continue;
            }

            $chart['ref_metric'] = $metric['ref_metric'];
            $chart['data'] = json_encode($metric['chart']['data']);
            $chart['chart_type'] = Arr::get($metric['chart'], 'chart_type');
            $chart['chart_stacked'] = Arr::get($metric['chart'], 'chart_stacked', 0);
            $chart['chart_horizontal'] = Arr::get($metric['chart'], 'chart_horizontal', 0);

            $charts[] = $chart;
        }

        $input = $charts;

        $validation = Validator::make($input, [
            '*.data.*' => 'required',
        ]);

        if ($validation->fails()) {
            http_response_code(422);
            die(json_encode($validation->errors()));
        }

        $data = [];

        foreach ($input as $record) {

            $new_record = [];

            foreach ($keys as $key) {
                
                $new_record[$key] = array_key_exists($key, $record)
                    ? $record[$key]
                    : null;
            }

            $new_record['user_id'] = $user_id;
            $new_record['cycle_name'] = $cycle_name;
            $new_record['created_at'] = date('Y-m-d H:i:s');
            $new_record['updated_at'] = date('Y-m-d H:i:s');

            $data[] = $new_record;
        }

        DB::table('pn_metric_charts')
            ->where('user_id', $user_id)
            ->where('cycle_name', $cycle_name)
            ->delete();

        DB::table('pn_metric_charts')
            ->insert($data);

        DB::table('pn_archive_metric_charts')
            ->insert($data);
    }

    public function storeScoreboardCheckin(Request $request) {

        $user_id = auth()->user()->id;
        $user_name = auth()->user()->name;
        $user_email = auth()->user()->email;
        $week_id = $request->input('week_id');
        $cycle_name = $request->input('cycle_name');
        $quarter_week = "$cycle_name W$week_id";

        $newRecord = ScoreboardCheckin::firstOrCreate([
            'user_id' => $user_id,
            'cycle_name' => $cycle_name,
            'week_id' => $week_id
        ]);

        $newRecord->count++;
        $newRecord->mail_sent = 0;
        $newRecord->notify_email_1 = $request->input('notify_email_1');
        $newRecord->notify_email_2 = $request->input('notify_email_2');
        $newRecord->notify_email_3 = $request->input('notify_email_3');
        $newRecord->save();
    }

    public function storeUserReports(Request $request) {

        $user = auth()->user();
        $week_id = $request->input('week_id');
        $cycle_name = $request->input('cycle_name');

        $wig2lag = DB::table('metrics as m')
            ->select(
                'm.user_id',
                'm.cycle_name',
                'mt.week_id',
                DB::raw("'WIG2LAG' report_key"),
                DB::raw("truncate(
                    avg(
                        if(
                            m.is_reverse_progress is null || m.is_reverse_progress = 0,
                            mt.track_value / mt.target_value,
                            (2 * mt.target_value - mt.track_value) / mt.target_value
                        )
                    ), 2
                ) report_value")
            )
            ->join('metrics as wig', function ($join) {
                $join->on('m.user_id', 'wig.user_id')
                    ->on('m.cycle_name', 'wig.cycle_name')
                    ->whereColumn('m.ref_wig', '>', 'wig.ref_wig')
                    ->where('wig.type_code', 'wig');
            })
            ->leftJoin('metric_tracks as mt', function ($join) {
                $join->on('m.user_id', 'mt.user_id')
                    ->on('m.cycle_name', 'mt.cycle_name')
                    ->on('m.ref_metric', 'mt.ref_metric');
            })
            ->where('m.user_id', $user->id)
            ->where('m.cycle_name', $cycle_name)
            ->where('m.type_code', 'lag')
            ->where('mt.week_id', $week_id)
            ->groupBy(
                'm.user_id',
                'm.cycle_name',
                'mt.week_id'
            )
            ->first();

        $wig2lead = DB::table('metrics as m')
            ->select(
                'm.user_id',
                'm.cycle_name',
                'mt.week_id',
                DB::raw("'WIG2LEAD' report_key"),
                DB::raw("truncate(
                    avg(
                        if(
                            m.is_reverse_progress is null || m.is_reverse_progress = 0,
                            mt.track_value / mt.target_value,
                            (2 * mt.target_value - mt.track_value) / mt.target_value
                        )
                    ), 2
                ) report_value")
            )
            ->join('metrics as wig', function ($join) {
                $join->on('m.user_id', 'wig.user_id')
                    ->on('m.cycle_name', 'wig.cycle_name')
                    ->whereColumn('m.ref_wig', '>', 'wig.ref_wig')
                    ->where('wig.type_code', 'wig');
            })
            ->leftJoin('metric_tracks as mt', function ($join) {
                $join->on('m.user_id', 'mt.user_id')
                    ->on('m.cycle_name', 'mt.cycle_name')
                    ->on('m.ref_metric', 'mt.ref_metric');
            })
            ->where('m.user_id', $user->id)
            ->where('m.cycle_name', $cycle_name)
            ->where('m.type_code', 'lead')
            ->where('mt.week_id', $week_id)
            ->groupBy(
                'm.user_id',
                'm.cycle_name',
                'mt.week_id'
            )
            ->first();

        $wig2act = DB::table('metrics as m')
            ->select(
                'm.user_id',
                'm.cycle_name',
                'mt.week_id',
                DB::raw("'WIG2ACT' report_key"),
                DB::raw("truncate(
                    avg(
                        if(
                            m.is_reverse_progress is null || m.is_reverse_progress = 0,
                            mt.track_value / mt.target_value,
                            (2 * mt.target_value - mt.track_value) / mt.target_value
                        )
                    ), 2
                ) report_value")
            )
            ->join('metrics as wig', function ($join) {
                $join->on('m.user_id', 'wig.user_id')
                    ->on('m.cycle_name', 'wig.cycle_name')
                    ->whereColumn('m.ref_wig', '>', 'wig.ref_wig')
                    ->where('wig.type_code', 'wig');
            })
            ->leftJoin('metric_tracks as mt', function ($join) {
                $join->on('m.user_id', 'mt.user_id')
                    ->on('m.cycle_name', 'mt.cycle_name')
                    ->on('m.ref_metric', 'mt.ref_metric');
            })
            ->where('m.user_id', $user->id)
            ->where('m.cycle_name', $cycle_name)
            ->where('m.type_code', 'act')
            ->where('mt.week_id', $week_id)
            ->groupBy(
                'm.user_id',
                'm.cycle_name',
                'mt.week_id'
            )
            ->first();

        $capa_open_count = DB::table('metric_actions')
            ->where('user_id', $user->id)
            ->where('cycle_name', $cycle_name)
            ->where(function ($query) {
                $query->where('type', 'Conversation')
                    ->orWhere('type', 'Feedback');
            })
            ->where('status', 1)
            ->count();

        $capa_complete_count = DB::table('metric_actions')
            ->where('user_id', $user->id)
            ->where('cycle_name', $cycle_name)
            ->where(function ($query) {
                $query->where('type', 'Conversation')
                    ->orWhere('type', 'Feedback');
            })
            ->where('status', 2)
            ->count();

        $recognition_count = DB::table('metric_actions')
            ->where('user_id', $user->id)
            ->where('cycle_name', $cycle_name)
            ->where('type', 'Recognition')
            ->count();

        $date = date('Y-m-d H:i:s');

        if ($wig2lag) {

            $update_wig2lag = DB::table('pn_user_report')
                ->updateOrInsert(
                    [
                        'user_id' => $user->id,
                        'cycle_name' => $cycle_name,
                        'week_id' => $week_id,
                        'report_key' => 'WIG2LAG'
                    ],
                    [
                        'report_value' => $wig2lag->report_value,
                        'created_at' => $date,
                        'updated_at' => $date
                    ]
                );
        }

        if ($wig2lead) {

            $update_wig2lead = DB::table('pn_user_report')
                ->updateOrInsert(
                    [
                        'user_id' => $user->id,
                        'cycle_name' => $cycle_name,
                        'week_id' => $week_id,
                        'report_key' => 'WIG2LEAD'
                    ],
                    [
                        'report_value' => $wig2lead->report_value,
                        'created_at' => $date,
                        'updated_at' => $date
                    ]
                );
        }

        if ($wig2act) {

            $update_wig2act = DB::table('pn_user_report')
                ->updateOrInsert(
                    [
                        'user_id' => $user->id,
                        'cycle_name' => $cycle_name,
                        'week_id' => $week_id,
                        'report_key' => 'WIG2ACT'
                    ],
                    [
                        'report_value' => $wig2act->report_value,
                        'created_at' => $date,
                        'updated_at' => $date
                    ]
                );
        }

        $update_capa_open_count = DB::table('pn_user_report')
            ->updateOrInsert(
                [
                    'user_id' => $user->id,
                    'cycle_name' => $cycle_name,
                    'week_id' => $week_id,
                    'report_key' => 'CAPA_OPEN'
                ],
                [
                    'report_value' => $capa_open_count,
                    'created_at' => $date,
                    'updated_at' => $date
                ]
            );

        $update_capa_complete_count = DB::table('pn_user_report')
            ->updateOrInsert(
                [
                    'user_id' => $user->id,
                    'cycle_name' => $cycle_name,
                    'week_id' => $week_id,
                    'report_key' => 'CAPA_COMPLETE'
                ],
                [
                    'report_value' => $capa_complete_count,
                    'created_at' => $date,
                    'updated_at' => $date
                ]
            );

        $update_recognition_count = DB::table('pn_user_report')
            ->updateOrInsert(
                [
                    'user_id' => $user->id,
                    'cycle_name' => $cycle_name,
                    'week_id' => $week_id,
                    'report_key' => 'RECOGNITION'
                ],
                [
                    'report_value' => $recognition_count,
                    'created_at' => $date,
                    'updated_at' => $date
                ]
            );

        // count improved wig metrics.
        // count unchanged wig metrics.
        // count decreased wig metrics.
        // count recognition received.
    }
}
