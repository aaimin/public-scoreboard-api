<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\MetricAction;
use Illuminate\Http\Request;

class MetricActionDestroyApiController extends Controller {

    public function __invoke(Request $request, $id) {

        $user_id = auth()->user()->id;
        $week_id = $request->query('week_id');
        $ref_metric = $request->query('ref_metric');

        MetricAction::where('id', $id)
            ->where('user_id', $user_id)
            ->where('week_id', $week_id)
            ->where('ref_metric', $ref_metric)
            ->delete();

        return [
            'status' => 'success'
        ];
    }
}
