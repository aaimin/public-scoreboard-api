<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Metric;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class MetricApiController extends Controller {

    public function index(Request $request) {

        $user = auth()->user();
        $cycle_name = $request->query('cycle_name');

        $metrics = DB::table('metrics as m')
            ->select(
                'm.*',
                'md.*',
                'm.user_id',
                'm.ref_metric',
                'm.cycle_name',
                'm.target_value',
                'mc.chart_type',
                'mc.chart_stacked',
                'mc.chart_horizontal',
                'mc.data as mc_data',
                'm.id'
            )
            ->leftJoin('pn_metric_details as md', function ($join) {
                $join->on('m.user_id', '=', 'md.user_id');
                $join->on('m.cycle_name', '=', 'md.cycle_name');
                $join->on('m.ref_metric', '=', 'md.ref_metric');
            })
            ->leftJoin('pn_metric_charts as mc', function ($join) {
                $join->on('m.user_id', '=', 'mc.user_id');
                $join->on('m.cycle_name', '=', 'mc.cycle_name');
                $join->on('m.ref_metric', '=', 'mc.ref_metric');
            })
            ->where('m.user_id', $user->id)
            ->where('m.cycle_name', $cycle_name)
            ->orderBy('ref_wig')
            ->orderBy('ref_lag')
            ->orderBy('ref_lead')
            ->orderBy('ref_act')
            ->orderBy('ref_qact')
            ->get();

        foreach ($metrics as $metric) {

            if ($metric->data) {
                $metric->detail = [
                    'data' => $metric->data,
                    // 'headers' => $metric->headers,
                    // 'data' => json_decode($metric->data),
                    // 'cellStyle' => json_decode($metric->cellStyle)
                ];
            }

            if ($metric->mc_data) {
                $metric->chart = [
                    'chart_type' => $metric->chart_type,
                    'chart_stacked' => $metric->chart_stacked,
                    'data' => json_decode($metric->mc_data),
                ];
            }

            unset($metric->data);
            unset($metric->headers);
            unset($metric->cellStyle);

            unset($metric->chart_type);
            unset($metric->chart_stacked);
            unset($metric->mc_data);
        }

        $response['status'] = 'success';
        $response['data']['metrics'] = $metrics;

        return $response;
    }
}
