<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use HasApiTokens, Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'google_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function detail () {
        return $this->hasOne('App\Models\UserDetail', 'id');
    }

    public function user_groups () {
        return $this->belongsToMany(
            'App\Models\UserGroup',
            'pn_user_user_groups'
        );
    }
}
