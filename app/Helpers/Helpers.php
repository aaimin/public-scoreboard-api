<?php

namespace App\Helpers;

use DateTime;

class Helpers {
    
    public static function getCurrentWeek() {

        $date_time = new DateTime();
        $date_time_day = (int) $date_time->format('N');
        $date_time_week = (int) $date_time->format('W');

        if ($date_time_day <= 4) {
            $date_time_week = $date_time_week - 1;
        }

        return $date_time_week;
    }
}