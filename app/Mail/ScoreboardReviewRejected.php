<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;

class ScoreboardReviewRejected extends Mailable  {

    use Queueable, SerializesModels;

    protected $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function build() {

        return $this
            ->subject(Arr::get($this->data, 'subject'))
            ->view('scoreboard-review-rejected')
            ->with($this->data);
    }
}
