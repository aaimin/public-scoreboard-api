<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Arr;

class ScoreboardCheckedIn extends Mailable {

    use Queueable, SerializesModels;

    protected $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function build() {

        return $this
            ->subject(Arr::get($this->data, 'subject'))
            ->view('scoreboard-checked-in')
            ->with($this->data);
    }
}
