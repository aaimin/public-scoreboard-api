<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MetricQuery extends Model {

    protected $table = 'pn_metric_queries';

    public function value_query () {
        return $this->belongsTo('App\Models\Query');
    }

    public function detail_query () {
        return $this->belongsTo('App\Models\Query');
    }
}
