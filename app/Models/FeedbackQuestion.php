<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeedbackQuestion extends Model {

    protected $table = 'pn_feedback_question';
}
