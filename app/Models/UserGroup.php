<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model {

    protected $table = 'pn_user_groups';

    public function pivot_users () {
        return $this->hasMany(
            'App\Models\UserUserGroup'
        );
    }

    public function users () {
        return $this->belongsToMany(
            'App\User',
            'pn_user_user_groups'
        );
    }
}
