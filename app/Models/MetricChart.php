<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;

class MetricCHart extends Model {

    use Compoships;

    protected $table = 'pn_metric_charts';
}
