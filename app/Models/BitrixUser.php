<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BitrixUser extends Model {

    protected $table = 'BitrixUsers';
}
