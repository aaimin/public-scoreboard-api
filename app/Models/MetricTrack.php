<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;

class MetricTrack extends Model {

    use Compoships;

    public function metric () {
        return $this->belongsTo(
            'App\Models\Metric',
            ['user_id', 'ref_metric', 'cycle_name'],
            ['user_id', 'ref_metric', 'cycle_name']
        );
    }

    public function user () {
        return $this->belongsTo('App\User');
    }
}
