<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MetricAction extends Model {

    protected $fillable = [
        'user_id',
        'cycle_name',
        'week_id',
        'ref_metric',
        'type',
        'issue',
        'capa',
        'accountable',
        'responsible',
        'notify_responsible',
        'deadline',
        'status',
        'completed_at',
        'created_at',
        'update_at'
    ];
}
