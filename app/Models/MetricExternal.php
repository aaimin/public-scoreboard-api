<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MetricExternal extends Model {

    protected $table = 'pn_metric_externals';
}
