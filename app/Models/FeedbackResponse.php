<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeedbackResponse extends Model {

    protected $table = 'pn_feedback_response';
}
