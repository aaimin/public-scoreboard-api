<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScoreboardCheckin extends Model {

    protected $table = 'pn_scoreboard_checkin';

    protected $guarded = [];
}
