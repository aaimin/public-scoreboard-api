<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MetricAggregate extends Model {

    protected $table = 'pn_metric_aggregates';
}
