<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScoreboardReview extends Model {

    protected $table = 'pn_scoreboard_review';

    protected $guarded = [];
}
