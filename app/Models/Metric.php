<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;

class Metric extends Model {

    use Compoships;

    public function detail () {
        return $this->hasOne('App\Models\MetricDetail',
            ['user_id', 'ref_metric', 'cycle_name'],
            ['user_id', 'ref_metric', 'cycle_name']
        );
    }

    public function chart () {
        return $this->hasOne('App\Models\MetricChart',
            ['user_id', 'ref_metric', 'cycle_name'],
            ['user_id', 'ref_metric', 'cycle_name']
        );
    }

    public function tracks () {
        return $this->hasMany('App\Models\MetricTrack',
            ['user_id', 'ref_metric', 'cycle_name'],
            ['user_id', 'ref_metric', 'cycle_name']
        )->orderBy('week_id');
    }
}
