<?php

namespace App\Actions;

use Illuminate\Support\Arr;
use GuzzleHttp\Client;

class BitrixTaskStoreAction {

    public function execute($data) {

        $title = Arr::get($data, 'title');
        $description = Arr::get($data, 'description');
        $responsible_id = Arr::get($data, 'responsible_id');
        $tags = Arr::get($data, 'tags');
        $deadline = Arr::get($data, 'deadline');

        if (!$title || !$responsible_id) {
            return false;
        }

        $group_id = 326;

        $client_id = 'local.5d8061b66128a4.77336484';
        $client_secret = 'JniIDjUGQ4f6pSB7rIdI9OYGeLmveeK2YTpTlL3nLaf8Vbgn6k';
        $refresh_token = '1d7beb5d0040471800354af600000264504603d4b3be6cd17f1facba41a230402830d3';

        $client = new Client();

        $token_response = $client->get('https://greenpacket.bitrix24.com/oauth/token', [
            'query' => [
                'scope' => 'granted_permission',
                'grant_type' => 'refresh_token',
                'client_id' => $client_id,
                'client_secret' => $client_secret,
                'refresh_token' => $refresh_token
            ]
        ]);

        $token_response_body = json_decode($token_response->getBody(), true);

        $access_token = $token_response_body['access_token'];

        $task_add_response = $client->get('https://greenpacket.bitrix24.com/rest/tasks.task.add', [
            'query' => [
                'auth' => $access_token,
                'fields[TITLE]' => $title,
                'fields[DESCRIPTION]' => $description,
                'fields[RESPONSIBLE_ID]' => $responsible_id,
                'fields[GROUP_ID]' => $group_id,
                'fields[TAGS]' => $tags,
            ]
        ]);

        $task_add_response_body = json_decode($task_add_response->getBody(), true);

        $taskId = Arr::get($task_add_response_body, 'result.task.id');

        $task_update_response = $client->get('https://greenpacket.bitrix24.com/rest/tasks.task.update', [
            'query' => [
                'auth' => $access_token,
                'taskId' => $taskId,
                'fields[DEADLINE]' => $deadline . ' 23:00'
            ]
        ]);

        $task_update_response_body = json_decode($task_update_response->getBody(), true);

        return true;
    }
}