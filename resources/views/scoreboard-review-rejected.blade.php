
<body>

<div>Hi,</div>

<br/>

<div>Your scoreboard review for <a href="{{ $url }}">{{ $cycle_name }}</a> has been rejected.</div>
<br/>

@if ($comment)

<div>{{ $comment }}</div>
<br/>

@endif

<div>From,</div>
<div>Performance Navigator</div>

</body>
