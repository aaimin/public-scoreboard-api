
<body>

<div>Hi {{ $responsible }},</div>

<br/>

<div>{{ $assigner }} has assigned a CAPARA for your action.</div>
<div>Below is the details:</div>

<br/>

<table>
    <tr>
        <td style="font-weight: bold">Task</td>
        <td>{{ $task }}</td>
    </tr>
    <tr>
        <td style="font-weight: bold">Description</td>
        <td>{{ $description }}</td>
    </tr>
    <tr>
        <td style="font-weight: bold">Deadline</td>
        <td>{{ $deadline }}</td>
    </tr>
</table>

<br>

<div>Please change task status to "Finish" after task is completed.</div>
<div>Thank you.</div>

<br/>

<div>Sent by Performance Navigator.</div>

<br/>

<div>Please do not reply.</div>

</body>
