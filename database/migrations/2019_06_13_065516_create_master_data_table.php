<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterDataTable extends Migration {

    public function up () {

        Schema::create('master_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('business_unit', 30);
            $table->string('group', 30);
            $table->string('function', 30);
            $table->string('name', 30);
            $table->string('cycle', 10);
            $table->string('code', 10);
            $table->string('type', 10);
            $table->text('metric');
            $table->string('date', 30);
            $table->string('period', 5);
            $table->integer('value');
            $table->integer('target');
            $table->integer('difference');
            $table->float('progress', 5, 2);
            $table->timestamps();
        });

        Schema::create('master_data_cfr', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('by', 30);
            $table->string('cycle', 10);
            $table->string('week', 5);
            $table->string('type', 10);
            $table->text('lag_name');
            $table->text('cfr_lesson_learned');
            $table->text('ca_pa_ra');
            $table->text('accountable');
            $table->text('responsible');
            $table->string('deadline', 30);
            $table->timestamps();
        });
    }

    public function down () {
        Schema::dropIfExists('master_data');
        Schema::dropIfExists('master_data_cfr');
    }
}
