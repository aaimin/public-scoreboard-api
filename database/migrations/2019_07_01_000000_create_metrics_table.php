<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetricsTable extends Migration {

    public function up() {
        Schema::create('metrics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('type_id');
            $table->string('type_code', 10);
            $table->tinyInteger('idx_wig');
            $table->tinyInteger('idx_lag');
            $table->tinyInteger('idx_lead');
            $table->tinyInteger('idx_act');
            $table->tinyInteger('idx_qact');
            $table->string('metric_code', 10);
            $table->string('metric_label');
            $table->text('metric_detail')->nullable();
            $table->string('target_unit', 10)->nullable();
            $table->float('target_value', 10, 2)->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('metrics');
    }
}
