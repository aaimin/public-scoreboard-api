<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePnStaffTmp extends Migration {

    public function up () {

        Schema::create('pn_staff_tmp', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('business_unit', 30);
            $table->string('group', 30);
            $table->string('function', 30);
            $table->string('name', 30);
            $table->timestamps();
        });
    }

    public function down () {
        Schema::dropIfExists('pn_staff_tmp');
    }
}
