<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePnMetricTmp extends Migration {

    public function up () {

        Schema::create('pn_metric_tmp', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('metric_name', 100);
            $table->string('type', 10);
            $table->string('code', 10);
            $table->string('lag_no', 3);
            $table->string('lead_no', 3);
            $table->string('act_no', 3);
            $table->string('owner_id', 30);
            $table->string('cycle', 10);
            $table->boolean('is_favourite');
            $table->timestamps();
        });
    }

    public function down () {
        Schema::dropIfExists('pn_metric_tmp');
    }
}
